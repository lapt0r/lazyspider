package cli

import (
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/lapt0r/lazyspider/spider"
	"gitlab.com/lapt0r/lazyspider/textparser"
)

const (
	shellOK = iota
	shellGenericError
	shellParseError
)

// Run runs the CLI implementation
func Run(args []string) int {
	if err := runInternal(args); err != nil {
		if err, ok := err.(interface{ ExitCode() int }); ok {
			return err.ExitCode()
		}
		return shellGenericError
	}
	return shellOK
}

func runInternal(args []string) (err error) {
	var targetUrl string
	var spiderType string
	var useJson bool
	var divPath string
	var keywords string
	var banwords string
	flag.StringVar(&targetUrl, "url", "", "The url to spider from")
	flag.StringVar(&spiderType, "spiderType", "lazy", "Spider type to use")
	flag.StringVar(&divPath, "selector", "", "JQuery-style selector for generic mode")
	flag.BoolVar(&useJson, "json", false, "pass the JSON flag to output a JSON list")
	flag.StringVar(&keywords, "keywords", "", "comma-separated list of keywords to search for in generic mode")
	flag.StringVar(&banwords, "banwords", "", "comma-separated list of banwords to ignore in generic mode")
	flag.Parse()
	switch strings.ToLower(spiderType) {
	case "lazy":
		return lazyCrawl(targetUrl, useJson)
	case "msdn":
		return msdnCrawl(targetUrl, useJson)
	case "generic":
		if len(divPath) == 0 {
			return errors.New("generic crawler specified but no divPath supplied. Supply a target div path with --divPath")
		} else {
			return genericCrawl(targetUrl, divPath, strings.Split(keywords, ","), strings.Split(banwords, ","), useJson)
		}
	default:
		return errors.New("unsupported crawler name supplied")
	}
}

func outputWordcloudResults(results []textparser.Item, useJson bool) error {
	if useJson {
		bytes, err := json.Marshal(results)
		if err != nil {
			return err
		}
		os.Stdout.Write(bytes)
	} else {
		fmt.Println("Found ", len(results), " results")
		for _, i := range results {
			fmt.Println(i.Word, ",", i.Count)
		}
	}
	return nil
}

func outputResults(results []string, useJson bool) error {
	if useJson {
		bytes, err := json.Marshal(results)
		if err != nil {
			return err
		}
		os.Stdout.Write(bytes)
	} else {
		fmt.Println("Found ", len(results), " results")
		for _, i := range results {
			fmt.Println(i)
		}
	}
	return nil
}

func lazyCrawl(targetUrl string, useJson bool) error {
	spider, err := spider.Crawl(targetUrl)
	if err != nil {
		return err
	}
	err = outputWordcloudResults(spider, useJson)
	if err != nil {
		return err
	}
	return nil
}

func msdnCrawl(targetUrl string, useJson bool) error {
	result, err := spider.MsdnCrawl(targetUrl)
	if err != nil {
		return err
	}
	err = outputResults(result, useJson)
	if err != nil {
		return err
	}
	return nil
}

func genericCrawl(targetUrl string, selector string, keywords []string, banwords []string, useJson bool) error {
	result, err := spider.GenericCrawl(targetUrl, selector, keywords, banwords)
	if err != nil {
		return err
	}
	err = outputResults(result, useJson)
	if err != nil {
		return err
	}
	return nil
}
