package main

import (
	"os"

	cli "gitlab.com/lapt0r/lazyspider/cli"
)

func main() {
	os.Exit(cli.Run(os.Args[1:]))
}
