module gitlab.com/lapt0r/lazyspider

go 1.16

require (
	github.com/gocolly/colly/v2 v2.1.0
	github.com/jdkato/prose/v2 v2.0.0
)
