package spider

import (
	"fmt"
	"log"
	"net/url"
	"sync"

	"github.com/gocolly/colly/v2"
)

var vCache sync.Map
var rCache sync.Map

func GenericCrawl(target string, selector string, keywords []string, banwords []string) ([]string, error) {
	logger := log.Default()
	results := make([]string, 0)
	dataSources := []string{"p:contains('%s')"}

	if len(banwords) > 0 {
		for _, banned := range banwords {
			bannedphrase := fmt.Sprintf("dd:not(:contains('%s'))", banned)
			dataSources = append(dataSources, bannedphrase+":contains('%s')")
		}
	} else {
		dataSources = append(dataSources, "dd:contains('%s')")
	}

	//we are collecting domain-specific keywords, restrict to target domain
	targetUrl, urlErr := url.Parse(target)
	if urlErr != nil {
		log.Fatal(urlErr)
	}
	targetUrl.Fragment = ""
	c := colly.NewCollector(
		colly.AllowedDomains(targetUrl.Hostname()),
		//colly.MaxDepth(8),
	)
	c.OnHTML(selector, func(h *colly.HTMLElement) {
		//found target div, check to see if we have keywords to look for
		foundTarget := false
		if len(keywords) > 0 {
			for _, keyword := range keywords {
				for _, source := range dataSources {
					result := h.DOM.Find(fmt.Sprintf(source, keyword))
					if len(result.Nodes) > 0 {
						foundTarget = true
					}
				}
			}
		} else {
			foundTarget = true
		}
		if foundTarget {
			//append to list of interesting URLs
			url := h.Request.URL.String()
			_, exists := rCache.Load(url)
			if !exists {
				rCache.Store(url, true)
				logger.Println("Found target item at:", url)
				results = append(results, url)
			}
		}
	})
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		absoluteUrl, urlParseErr := targetUrl.Parse(link)
		if urlParseErr != nil {
			logger.Println("parser error:", urlParseErr)
		}
		if absoluteUrl != nil {
			toVisit := absoluteUrl.String()
			//don't duplicate work
			_, exists := vCache.Load(toVisit)
			if !exists {
				vCache.Store(toVisit, true)
				e.Request.Visit(toVisit)
			}
		}
	})

	err := c.Visit(targetUrl.String())
	if err != nil {
		logger.Println("error: ", err)
	}
	return results, err
}
