package spider

import (
	"log"
	"net/url"
	"regexp"

	"github.com/gocolly/colly/v2"
)

func MsdnCrawl(target string) ([]string, error) {
	logger := log.Default()
	qsRegex := regexp.MustCompile("view=net-[[:digit:]][:punct:]][[:digit:]]")
	results := make([]string, 0)
	cache := map[string]bool{}
	//we are collecting domain-specific keywords, restrict to target domain
	targetUrl, urlErr := url.Parse(target)
	if urlErr != nil {
		log.Fatal(urlErr)
	}
	c := colly.NewCollector(
		colly.AllowedDomains(targetUrl.Hostname()),
		//colly.MaxDepth(8),
	)

	c.OnHTML("div.WARNING", func(h *colly.HTMLElement) {
		//found a MSDN warning box, append to list of interesting URLs
		results = append(results, h.Request.URL.String())
	})
	c.OnHTML("a", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		//must be API docs
		_, exists := cache[link]
		if !exists && qsRegex.MatchString(link) {
			cache[link] = true
			e.Request.Visit(link)
		}
	})
	err := c.Visit(target)
	if err != nil {
		logger.Println("error: ", err)
	}
	return results, err
}
