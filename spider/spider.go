package spider

import (
	"fmt"
	"log"
	"math/rand"
	"net/url"
	"path"
	"time"

	"github.com/gocolly/colly/v2"
	"gitlab.com/lapt0r/lazyspider/textparser"
)

// Crawl crawls a specified URl
func Crawl(target string) ([]textparser.Item, error) {
	//todo: proper logger
	logger := log.Default()
	//we are collecting domain-specific keywords, restrict to target domain
	targetUrl, urlErr := url.Parse(target)
	if urlErr != nil {
		log.Fatal(urlErr)
	}

	c := colly.NewCollector(
		colly.AllowedDomains(targetUrl.Hostname()),
		colly.MaxDepth(6),
	)
	rand.Seed(time.Now().UnixNano())
	nounMap := make(map[string]int)
	result := make([]string, 0)
	parents := make(map[string]float64)

	// mark 404s
	c.OnResponse(func(r *colly.Response) {
		if r.StatusCode == 404 {
			logger.Println("404 received for ", target)
		}
	})
	// Find and visit all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		link := e.Attr("href")
		parsed, err := url.Parse(link)
		if err == nil {
			parent := path.Dir(parsed.Path)
			threshold, exists := parents[parent]
			if !exists {
				parents[parent] = 1.0
			}
			//todo: configurable decays
			threshold = threshold * 0.25
			//If the parent directory is new, always crawl this.  Otherwise, respect decay
			if !exists || threshold > rand.Float64() {
				if parsed.Host != "" {
					result = append(result, fmt.Sprint(parsed))
				} else {
					result = append(result, fmt.Sprintf("%v%v", target, link))
				}
				e.Request.Visit(link)
			}
		}
	})

	c.OnHTML("body", func(e *colly.HTMLElement) {
		content := e.Text
		nouns := textparser.ExtractNouns(content)
		nounMap = textparser.Reduce(nounMap, nouns)
		logger.Println("Nouns found: ", nouns)
	})

	c.OnRequest(func(r *colly.Request) {
		logger.Println("Visiting", r.URL)
	})

	err := c.Visit(target)
	if err != nil {
		logger.Println("error: ", err)
	}
	nouncloud := textparser.SortCloud(textparser.MakeWordCloud(nounMap))
	return nouncloud, err
}
