package textparser

import (
	"log"
	"regexp"
	"sort"
	"strings"
	"sync"

	"github.com/jdkato/prose/v2"
)

type Item struct {
	Word  string
	Count int
}

// ExtractNouns extracts the nouns from a piece of text
func ExtractNouns(text string) map[string]int {
	doc, err := prose.NewDocument(text)
	if err != nil {
		log.Fatal(err)
	}
	sentences := doc.Sentences()
	result := make(map[string]int)
	sChannel := make(chan string)
	var wg sync.WaitGroup
	var qwg sync.WaitGroup
	for _, s := range sentences {
		wg.Add(1)
		go processSentence(s.Text, sChannel, &wg)
	}
	qwg.Add(1)
	go func(c chan string, wg *sync.WaitGroup) {
		defer qwg.Done()
		for {
			k, cok := <-c
			if !cok {
				break
			}
			elem, ok := result[k]
			if ok {
				elem++
				result[k] = elem
			} else {
				result[k] = 1
			}
		}
	}(sChannel, &qwg)
	wg.Wait()
	close(sChannel)

	//need to wait for buffer thread to safely exit or you get a map race
	//better approach?  WG is pretty heavy-handed for a single background thread
	qwg.Wait()
	return result
}

// MakeWordCloud converts a map (easier to manage duplicates) to an item slice (better for sorting)
func MakeWordCloud(stringMap map[string]int) []Item {
	result := make([]Item, len(stringMap))
	index := 0
	for k, v := range stringMap {
		result[index] = Item{
			Word:  k,
			Count: v,
		}
		index++
	}
	return result
}

func SortCloud(cloud []Item) []Item {
	//This is kind of a dirty hack courtesy of Steven Penny https://stackoverflow.com/questions/18695346/how-to-sort-a-mapstringint-by-its-values
	//tbd: better solution, inline anonymous function is hard to maintain & reason about
	sortFn := func(n, m int) bool {
		//Wordclouds sort in reverse order!  More count, more better!
		return cloud[n].Count > cloud[m].Count
	}

	//I'm pretty sure this violates some mutability property somewhere but we'll roll with it for now
	sort.Slice(cloud, sortFn)
	return cloud
}

//Reduce merges two [string]int maps
func Reduce(m1, m2 map[string]int) map[string]int {
	for k, v := range m1 {
		if v2, ok := m2[k]; ok {
			v += v2
		}
		m2[k] = v
	}
	return m2
}

func processSentence(sentence string, tchannel chan string, wg *sync.WaitGroup) {
	defer wg.Done()
	doc, err := prose.NewDocument(sentence)
	if err != nil {
		log.Fatal(err)
	}

	re := regexp.MustCompile(`^N`) //noun tags in Prose will all match N*
	for _, tok := range doc.Tokens() {
		if re.MatchString(tok.Tag) {
			//stripping common not-useful padding characters.  Integers aren't interesting to us (dumb fuzzing can find this)
			//we are also not interested in casing
			item := strings.ToLower(strings.Trim(tok.Text, " \t\"'|{}!?[]#:;.=0123456789"))
			if item != "" {
				tchannel <- item
			}
		}
	}
}
