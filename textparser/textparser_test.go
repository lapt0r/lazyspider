package textparser

import (
	"reflect"
	"testing"
)

func TestReduce(t *testing.T) {
	type args struct {
		m1 map[string]int
		m2 map[string]int
	}
	tests := []struct {
		name string
		args args
		want map[string]int
	}{
		// TODO: Add test cases.
		{
			name: "Simple Map Merge",
			args: args{
				m1: map[string]int{"foo": 1, "bar": 2},
				m2: map[string]int{"biz": 1, "baz": 2},
			},
			want: map[string]int{"foo": 1, "bar": 2, "biz": 1, "baz": 2},
		},
		{
			name: "Repeat Items Map Merge",
			args: args{
				m1: map[string]int{"foo": 1, "bar": 2},
				m2: map[string]int{"foo": 1, "bar": 2},
			},
			want: map[string]int{"foo": 2, "bar": 4},
		},
		{
			name: "Heterogeneous Items Map Merge",
			args: args{
				m1: map[string]int{"foo": 1},
				m2: map[string]int{"foo": 1, "bar": 2},
			},
			want: map[string]int{"foo": 2, "bar": 2},
		},
		{
			name: "Left Empty",
			args: args{
				m1: map[string]int{},
				m2: map[string]int{"foo": 1, "bar": 2},
			},
			want: map[string]int{"foo": 1, "bar": 2},
		},
		{
			name: "Right Empty",
			args: args{
				m1: map[string]int{"foo": 1, "bar": 2},
				m2: map[string]int{},
			},
			want: map[string]int{"foo": 1, "bar": 2},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Reduce(tt.args.m1, tt.args.m2); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Reduce() = %v, want %v", got, tt.want)
			}
		})
	}
}
